import os
import sys
from flask import Flask
from flask import render_template
from flask import request
from backpropagation import network
from werkzeug.utils import secure_filename
from backpropagation import mnistloader
import pickle as pcl

sys.modules['network'] = network
UPLOAD_FOLDER = 'images/'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'upload/'


@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        myfile = request.files['myFile']
        filename = secure_filename(myfile.filename)
        myfile.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        value = request.form['myValue']

        with open('backpropagation/network', 'rb') as f:
            net = pcl.load(f)
            test_data = mnistloader.load_from_image('{0}/{1}'.format(app.config['UPLOAD_FOLDER'], filename), int(value))
            result = net.recognize_single(test_data)

        return render_template('index.html', result=result)

    else:
        return render_template('index.html')


if __name__ == "__main__":
    app.run(debug=True)
