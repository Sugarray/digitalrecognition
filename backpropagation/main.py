import pickle as pcl

import mnistloader, network

if __name__ == '__main__':

    # training_data, validation_data, test_data = mnistloader.load_data_wrapper()
    training_data, validation_data, td = mnistloader.load_data_wrapper()
    test_data = mnistloader.load_from_image('images/660.png', 6)

    # 784 vhodnih 30 skritih 10 vihodnih
    net = network.Network([784, 30, 10])



    # with open('network', 'rb') as f:
    #     net = pcl.load(f)


    # ttt = []
    #
    # for i in td[1][0]:
    #     ttt.append(int(i[0] * 255))
    #
    # im = Image.new("L", (28, 28), "white")
    # im.putdata(ttt)
    # im.save("new.png")

    # epochs test_data batch_size
    net.SGD(training_data, 30, 10, 3.0, test_data=td)

    with open('network', 'wb') as f:
        pcl.dump(net, f)

    net.recognize_single(test_data)
